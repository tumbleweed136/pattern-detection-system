Start: 

CPE 185 Group Project Sequence Detector
Paul Dye Jesus Beltran Micah Biggers Durvin Barrios

*All source files are in downloads section*

Intro:
This is our final report for our project. In this file the basic project and details are described.
Additional source files are incuded in downloads. 

Descrption:
The parallax board generates a random sequence from a set length. The user tries to match the sequence
by pushing the corresponding LED in the same order. The user has a 20 second countdown to input their
sequence if not the game is over. If the user inputs an incorrect LED the game is also over.
Once the user enters the correct sequence the game is over and they win. The raspberry pi displays 
which LED is on or off and when the user wins or losses the pi will display the corresponding message.


Work Divided:
Paul Dye: 
I worked on the hardware initially designing the circuit. I also assisted Micah in some Python code. 
I also made a LED display to be a counter. 
Jesus Beltan:
Micah Biggers:
Durvin Barrios:


Challenges:
Paul Dye:
There was not enough inputs/outputs on the parralax board so I had to send a 3 bit signal 
from one parralax board to another and display the countdown timer. 
Jesus Beltan:
Micah Biggers:
Durvin Barrios:


Conclusion:
